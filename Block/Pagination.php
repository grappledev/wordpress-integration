<?php
namespace Ponderosa\WordpressIntegration\Block;

class Pagination extends \Magento\Framework\View\Element\Template
{
    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\View\Element\Template\Context $context,
        \Ponderosa\WordpressIntegration\Helper\Api $apiHelper
    ) {
        parent::__construct($context, []);
        $this->apiHelper = $apiHelper;
        $this->request = $request;
    }

    public function getCurrentPage()
    {
        $page = is_numeric($this->request->getParam('page')) && intval($this->request->getParam('page')) > 0 ? $this->request->getParam('page') : 1;

        return $page;
    }

    public function getPaginationLinks()
    {
        $numLinks = $this->apiHelper->getNumPaginationLinks();
        $currentPage = $this->getCurrentPage();
        $category = is_numeric($this->request->getParam('category')) ? intval($this->request->getParam('category')) : null;
        if($category):
            $allCategorisedPosts = $this->apiHelper->getAllPostsByCategory($category);
            $pagedPosts = $this->apiHelper->pageResources($allCategorisedPosts);
            $numPages = count($pagedPosts);
        else:
            $numPages = $this->apiHelper->getNumPages('posts');
        endif;

        $prevLink = '?page=' . ($currentPage - 1);
        $nextLink = '?page=' . ($currentPage + 1);

        if ($category):
            $prevLink .= '&category=' . $category;
            $nextLink .= '&category=' . $category;
        endif;

        $prevNavigation = $currentPage > 1 ? '<div class="ponderosa-pagination-links__navigation ponderosa-pagination-links__navigation--prev"><a href="'.$prevLink.'">Prev</a></div>' : '';
        $nextNavigation = $currentPage < $numPages ? '<div class="ponderosa-pagination-links__navigation ponderosa-pagination-links__navigation--next"><a href="'.$nextLink.'">Next</a></div>' : '';

        $pagination = $prevNavigation;

        if($currentPage <= $numLinks){
            $currentLink = 1;
        }
        else {
            $currentLink = $currentPage % $numLinks > 0 ? (floor($currentPage / $numLinks) * $numLinks) + 1 : (($currentPage / $numLinks - 1) * $numLinks) + 1;
        }

        for ($i=0; $i < $numLinks ; $i++) {

            if($currentLink > $numPages){
                break;
            }

            $href = '?page='.$currentLink;
            if($category):
                $href.= '&category='.$category;
            endif;

            $class = $currentLink == $currentPage ? 'ponderosa-pagination-links__link ponderosa-pagination-links__link--active' : 'ponderosa-pagination-links__link';
            $pagination.= '<div class="'.$class.'"><a href="'.$href.'">'.$currentLink.'</a></div>';

            $currentLink++;
        }

        $pagination.=$nextNavigation;

        return $pagination;
    }

}
