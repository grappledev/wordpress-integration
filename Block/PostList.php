<?php
namespace Ponderosa\WordpressIntegration\Block;

class PostList extends \Magento\Framework\View\Element\Template
{
    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Controller\ResultFactory $resultFactory,
        \Ponderosa\WordpressIntegration\Helper\Api $apiHelper,
        \Magento\Framework\View\Page\Config $pageConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        parent::__construct($context, []);
        $this->apiHelper = $apiHelper;
        $this->resultFactory = $resultFactory;
        $this->request = $request;
        $this->pageConfig = $pageConfig;
        $this->storeManager = $storeManager;
    }

    public function getApiUrl()
    {
        return $this->apiHelper->getResourcesApiUrl('posts');
    }

    public function getLatestPosts()
    {
        $category = is_numeric($this->request->getParam('category')) ? intval($this->request->getParam('category')) : null;
        $page = is_numeric($this->request->getParam('page')) && intval($this->request->getParam('page')) > 0 ? $this->request->getParam('page') : 1;

        if($category):
            $categorisedPosts = $this->apiHelper->getAllPostsByCategory($category);
            $pagedData = $this->apiHelper->pageResources($categorisedPosts);
            $result = count($pagedData) ? json_encode($pagedData[$page - 1]) : json_encode([]);
        else:
            $result = $this->apiHelper->getLatestResources('posts', $page);
        endif;

        return $result;
    }

    public function scrapeImageFromContent( $content )
    {
        return $this->apiHelper->scrapeImageFromContent($content);
    }

    public function getCategoryNicename ( int $id ){

        $category = $this->apiHelper->getResourceById('categories', $id);
        return $category->name;
    }

    public function getMaxPosts()
    {
        return $this->apiHelper->getMaxPosts();
    }

    public function getMetaTitle()
    {
        // $this->storeManager->getStore()->getName().' | '.ucwords(str_replace('-',' ',$this->apiHelper->getBlogSlug()));
        return ucwords(str_replace('-',' ',$this->apiHelper->getBlogSlug()));
    }

    protected function _prepareLayout()
    {
        $this->pageConfig->getTitle()->set($this->getMetaTitle());

        return parent::_prepareLayout();
    }
}
