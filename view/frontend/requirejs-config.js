var config = {
    paths : {
        'swiper': [
            '//unpkg.com/swiper/swiper-bundle.min',
            'Ponderosa_WordpressIntegration/js/lib/swiper.min'
        ]
    },
    map: {
        '*': {
            latestPosts: 'Ponderosa_WordpressIntegration/js/latestPosts',
            articleSlider: 'Ponderosa_WordpressIntegration/js/articleSlider'
        }
    },
    shim: {
        'articleSlider' : {
            deps: ['swiper']
        }
    }
};