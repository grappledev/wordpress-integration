define([
    "jquery",
    "swiper"
], function ($, Swiper) {
    "use strict";
    return function (config, element) {

        const swiper = new Swiper('.ponderosa-more-slider__slider', {
            // Optional parameters
            direction: 'horizontal',
            slidesPerView: 2,
            loop: true,

            // // If we need pagination
            // pagination: {
            //     el: '.swiper-pagination',
            // },

            // Navigation arrows
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

            // // And if we need scrollbar
            // scrollbar: {
            //     el: '.swiper-scrollbar',
            // },
        });

    }
})